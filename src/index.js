let section="people";
let table_name="data_table";
let modo="1_hover";

let people=[
    {nombre:"David",apellido:"Pineda",edad:36,nacionalidad:"Chileno"},
    {nombre:"Raquel", apellido:"Bracho", edad:25, nacionalidad:"Venezolana"},
    {nombre:"Daniel", apellido:"Martinez", edad:45, nacionalidad:"Ecuatoriano"},
    {nombre:"María", apellido:"Soto", edad:21, nacionalidad:"Argentina"},
    {nombre:"Francisco", apellido:"González", edad:34, nacionalidad:"Colombiano"},
    {nombre:"Carolina", apellido:"Herrera", edad:55, nacionalidad:"Española"}
];
//odd even

function oddOrEven(x) {
  return ( x & 1 ) ? "odd" : "even";
}
// order by>

function compare(a,b){
    if (a.edad>b.edad){return 1;};
    if (a.edad>b.edad){return -1;};
    return 0;};

people.sort(compare);

function add_data2row(row, headers, data){
    for (let item=0; item<headers.length;item++){
        let item_name=headers[item].textContent.toLowerCase();
        let elem_value=data[item_name];
        let cell=row.insertCell(item);
        cell.innerHTML=elem_value;
    };
};

function get_css_id(data, selection){
    let values=[];
    selection.forEach(e=>{values.push(data[e].toLowerCase());});
    return values.join("-");
};

function complete_table(section, table_name, data, opts){
    let block=document.getElementById(section);
    let table=block.getElementsByClassName(table_name)[0];
    let headers=table.getElementsByTagName('thead')[0].getElementsByTagName('th');
    let table_body=table.getElementsByTagName('tbody')[0];
    let selection=opts.selection;
    for (let index in data){
        let row=table_body.insertRow(parseInt(index));
        let class_name=oddOrEven(index);
        row.setAttribute('class', class_name);
        row.setAttribute('id', get_css_id(data[index], selection));
        row.addEventListener('click', function(){
            let info=block.getElementsByClassName('data_info')[0].lastElementChild;
            info.innerHTML=JSON.stringify(data[index]);
        });
        add_data2row(row, headers, data[index]);
    };
}


complete_table('people','data_table', people, {selection:["nombre", "apellido"]});
